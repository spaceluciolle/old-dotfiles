# MPD daemon start (if no other user instance exists)
[[ -f ~/.config/mpd/pid  ]] && ps -ef | grep $(cat ~/.config/mpd/pid) | grep mpd$ > /dev/null || mpd
# start ssh-agent if not already started
[[ ! -z ${SSH_AGENT_PID} ]] && ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || eval $(ssh-agent)
export TERMINAL=st
export EDITOR=vim
