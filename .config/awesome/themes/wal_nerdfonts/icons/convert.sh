#!/bin/bash
source ~/.cache/wal/colors.sh
basecolor='#DCDCCC'

layouts=$(find ./samples/layouts -name '*.png')
if [ ! -d ./layouts ]
then
    mkdir ./layouts
fi

for layout in $layouts
do
    filename="${layout##*/}"
    convert $layout -fill $foreground -opaque $basecolor layouts/$filename
done

if [ ! -d ./titlebar ]
then
    mkdir ./titlebar
fi

titlebaricons=$(find ./samples/titlebar -name '*.png')

for titlebaricon in $titlebaricons
do
    filename="${titlebaricon##*/}"
    fillcolor=$foreground
    if [[ $filename = *"focus"* ]]
    then
        color='#46a8c3'
        fillcolor=$color7
        if [[ $filename = *"inactive"* ]]; then
            color='#bdbdbd'
            fillcolor=$background
        fi
    else
        color='#61a32e'
        fillcolor=$color3
        if [[ $filename = *"inactive"* ]]; then
            color='#cccccc'
            fillcolor=$foreground
        fi
    fi
    convert $titlebaricon -fill $fillcolor -opaque $color titlebar/$filename
done
