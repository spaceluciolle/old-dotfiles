#!/usr/bin/env bash
OPTION="-a 90 -q"
function main() {
  main_menu

  oomox-cli -o wal ~/.cache/wal/colors-oomox
  oomox-archdroid-icons-cli  -o wal ~/.cache/wal/colors-oomox 
  cd ~/.config/awesome/themes/wal_nerdfonts/icons/ && ./convert.sh 
  wmreload
}

function main_menu(){
  type="$(echo -e "theme\nwallpaper" | rofi -dmenu -p "Generate theme with a ")"
  case $type in
    theme)
      with_theme
      ;;
    wallpaper)
      with_wallpaper
      ;;
  esac
}

function with_wallpaper() {
  wallpaper=$(ls ~/Images/wallpaper | rofi -dmenu -p 'choose a wallpaper')
  [[ -z $wallpaper ]] && exit 0
  if [[ -n $wallpaper ]]; then
    backend=$(wal --backend | grep '-' | awk '{print $2}' | rofi -dmenu -p 'choose a backend')
    [[ -z $backend ]] && exit 0
    wal $OPTION --backend $backend -i ~/Images/wallpaper/$wallpaper
  fi
}
function with_theme() {
  action="Light\nDark"
  light_or_dark=$(echo -e $action | rofi -dmenu -p "Dark or light ")
  [[ -z $light_or_dark ]] && exit 0
  theme_list=$(wal --theme)
  start=$(echo -e "$theme_list" | grep -n "$light_or_dark" | awk -F ":" '{print $1}')
  theme_list=$(echo -e "$theme_list" | tail -n+$(($start+1)))
  end=$(echo "$theme_list" | grep -n ":" | awk -F ":" '{print $1}' | head -n1)
  theme_list=$(echo -e "$theme_list" | head -n+$(($end-1)))
  theme=$(echo "$theme_list" | awk '{print $2}' | rofi -dmenu "Choose a theme")
  [[ -z $theme ]] && exit 0
  if [[ $light_or_dark == "Light" ]]; then
    OPTION="$OPTION -l"
  fi
  wal $OPTION -f $theme 
}

main
