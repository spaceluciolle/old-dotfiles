#!/bin/bash
source ~/.local/share/fonts/i_all.sh
source ~/.cache/wal/colors.sh
readonly ICON_COLOR="$color2"
readonly ACTIVE_COLOR="$color1"
readonly NUMBER_OF_DESKTOPS="$(xprop -root _NET_NUMBER_OF_DESKTOPS | awk '{print $3}')"
# Define the clock
Clock() {
  datetime=$(date "+%d/%m/%Y, %H:%M")
  echo -n "%{F$ICON_COLOR}$i_mdi_calendar_clock%{F-} $datetime"
}

#Define the battery
Battery() {
  batpercs="$(cat /sys/class/power_supply/BAT*/capacity)"
  batt=0
  batperc=""
  baticon=""
  while IFS= read -r perc; do
    if ((perc <= 5))
    then
      baticon="$i_fa_battery_empty"
    elif ((perc <= 15))
    then
      baticon="$i_fa_battery_quarter"
    elif ((perc <= 50))
    then
      baticon="$i_fa_battery_half"
    elif ((perc <= 75))
    then
      baticon="$i_fa_battery_three_quarters"
    else
      baticon="$i_fa_battery_full"
    fi

    batperc="$batperc %{F$ICON_COLOR}$baticon%{F-} BAT$batt: $perc%"
    batt=$((batt+1))
  done <<< "$batpercs"

  echo "$batperc"
}
Backlight(){
  brightness="$(echo "($(xbacklight -get)+0.5)/1" | bc)"
  if ((brightness <= 25)); then
    brightness="%{F$ICON_COLOR}$i_mdi_lightbulb_outline%{F-} $brightness"
  elif ((brightness <= 75)); then
    brightness="%{F$ICON_COLOR}$i_mdi_lightbulb_on_outline%{F-} $brightness"
  else
    brightness="%{F$ICON_COLOR}$i_mdi_lightbulb_on%{F-} $brightness"
  fi
  echo "$brightness%" 
}

Tag(){
  current="$(xprop  -root _NET_CURRENT_DESKTOP | awk '{print $3}')"
  tags=""
  for tag in $(seq 1 $NUMBER_OF_DESKTOPS)
  do
    if [ $tag -eq $((current+1)) ]
    then
      tags="$tags%{B$ACTIVE_COLOR} $tag %{B-}"
    else
      tags="$tags%{A:wmctrl -s $((tag-1)):}$tag%{A}"
    fi
  done
  echo $tags
}

Volume(){
  volume=$(pamixer --get-volume)
  if [[ $(pamixer --get-mute) == "true" ]] || ((volume == 0)); then
    volume="%{F$ICON_COLOR}$i_fa_volume_off%{F-} $volume"
  elif ((volume <= 50)) ; then 
    volume="%{F$ICON_COLOR}$i_fa_volume_down%{F-} $volume"
  else
    volume="%{F$ICON_COLOR}$i_fa_volume_up%{F-} $volume"
  fi
  echo " $volume%"
}
Keyboard(){
  echo "%{A:xkb-switch -n:}%{F$ICON_COLOR}$i_mdi_keyboard_variant%{F-} $(xkb-switch)%{A}"
}

Cpu(){
  while true; do
    echo "%{F$ICON_COLOR}$i_mdi_chip%{F-} $(ps -eo pcpu | awk 'BEGIN {sum=0.0f} {sum+=$1} END {print sum}')%" > /tmp/bar/cpuusage
    sleep 2
  done
}

Fetchcpu(){
  cat /tmp/bar/cpuusage 
}

init(){
  if [ ! -d /tmp/bar ]; then
    mkdir /tmp/bar
  fi
  Cpu > /dev/null 2>&1 &
  while true; do
    echo -e "%{F$ICON_COLOR}$i_linux_archlinux%{F-}$(Tag) %{c} $(Clock) %{r}$(Keyboard) $(Fetchcpu) $(Backlight) $(Volume) $(Battery) "
  sleep 1
  done
}
init | lemonbar -f "Hack Nerd Font-9.5" -B "$background" | sh
